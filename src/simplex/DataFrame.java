package simplex;

import java.util.ArrayList;
import java.util.List;

public class DataFrame {
    public DataFrame(double[] x, double[] y) {
        _x = x;
        _y = y;
    }
    private double[] _x;
    private double[] _y;
    public int size() {
        return _x.length;
    }
    public double[] x() {
        return _x;
    }
    public double[] y() {
        return _y;
    }
}
