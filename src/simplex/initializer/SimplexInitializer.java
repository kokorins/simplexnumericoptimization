package simplex.initializer;

public interface SimplexInitializer {
    double[][] initSimplex(double[] InitHillParam);
}
