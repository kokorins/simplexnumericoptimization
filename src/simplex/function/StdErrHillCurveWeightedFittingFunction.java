package simplex.function;

import simplex.DataFrame;

public class StdErrHillCurveWeightedFittingFunction extends IFunction {
    final private IFitter _fitter;
    final private DataFrame _data;
    final private double[] _sigmas;

    public StdErrHillCurveWeightedFittingFunction(IFitter fitter, DataFrame data, double[] sigmas) {
        _fitter = fitter;
        _data = data;
        _sigmas = sigmas;
    }

    @Override
    public double calc(double[] x) {
        double res = 0;
        for (int i = 0; i < _data.x().length; ++i) {
            double elem = _data.y()[i] - _fitter.calc(_data.x()[i], x);
            res += elem * elem * _sigmas[i];
        }
        return res;
    }
}
