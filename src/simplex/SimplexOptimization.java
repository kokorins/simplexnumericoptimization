package simplex;

import simplex.averager.IWeightedSimplexAverage;
import simplex.averager.NonWeightedSimplexAverage;
import simplex.function.IFunction;
import simplex.parameters.SimplexParameters;
import simplex.parameters.SimplexStepParameters;

public class SimplexOptimization {

    private final static int minInd = 0;
    private final static int maxInd = 1;
    private final static int preMaxInd = 2;

    public static double[] LinearTransformVector(double[] origVect, double[] direcrVect, double aCoeff) {
        int Len = origVect.length;
        double[] result = new double[Len];
        for (int i = 0; i <= Len - 1; ++i)
            result[i] = origVect[i] + aCoeff * (direcrVect[i] - origVect[i]);
        return result;
    }

    private static double SimplexStep(SimplexStepParameters params, double[][] simplexVert, double[] funcOnSimpl, int[] idxes, IFunction function) {
        IWeightedSimplexAverage weightedSimplex = new NonWeightedSimplexAverage();
        double[] averageVert = weightedSimplex.averageVect(simplexVert, funcOnSimpl, idxes[maxInd]);
        double[] reflectVert = LinearTransformVector(averageVert, simplexVert[idxes[maxInd]], -params.reflCoeff);
        double reflectValue = function.calc(reflectVert);
        if (reflectValue < funcOnSimpl[idxes[minInd]]) {//Reflected value less then minimal one
            double[] expandVert = LinearTransformVector(averageVert, reflectVert, params.expandCoeff); // elongate reflected vector
            double expandValue = function.calc(expandVert);
            if (expandValue < reflectValue)
                simplexVert[idxes[maxInd]] = expandVert;
            else
                simplexVert[idxes[maxInd]] = reflectVert;
        } else {//ReflectValue >= aFuncOnSimpl[MinInd]
            if (reflectValue < funcOnSimpl[preMaxInd])
                simplexVert[idxes[maxInd]] = reflectVert;
            else {
                if (reflectValue < funcOnSimpl[idxes[maxInd]]) {
                    double[] contractVert = LinearTransformVector(averageVert, reflectVert, params.contractCoeff);
                    double contractValue = function.calc(contractVert);
                    if (contractValue < reflectValue)
                        simplexVert[idxes[maxInd]] = contractVert;
                    else
                        simplexVert[idxes[maxInd]] = reflectVert;
                } else {
                    double[] contractVert = LinearTransformVector(averageVert, simplexVert[idxes[maxInd]], params.contractCoeff);
                    double contractValue = function.calc(contractVert);
                    if (contractValue < funcOnSimpl[idxes[maxInd]])
                        simplexVert[idxes[maxInd]] = contractVert;
                    else
                        ShrinkSimplex(simplexVert, params.shrinkCoeff, idxes[minInd]);
                }
            }
        }
        funcOnSimpl[idxes[maxInd]] = function.calc(simplexVert[idxes[maxInd]]);
        return SimplexFuncSE(funcOnSimpl);
    }

    public static SimplexResult SimplexMethod(SimplexParameters params, double[][] simplexVertices, IFunction function) {
        double[] funcOnSimpl = FuncOnSimplex(simplexVertices, function);
        int iter = 0;
        double stdErr;
        int[] idxes;
        do {
            //minimal, maximal, before maximal
            idxes = GetMinMaxVertexInd(funcOnSimpl);
            stdErr = SimplexStep(params.getStepParameters(), simplexVertices, funcOnSimpl, idxes, function);
            ++iter;
        } while (iter <= params.getNumIterations() && stdErr > params.getMaxAbsErr());

        double[] optimalParams = simplexVertices[idxes[minInd]];
        double minFuncValue = function.calc(optimalParams);
        return new SimplexResult(minFuncValue, optimalParams, iter);
    }

    public static int[] GetMinMaxVertexInd(double[] vertexValues) {
        int aMinIndex = 0;
        int aMaxIndex = aMinIndex;
        int aPreMaxIndex = aMinIndex;
        double MinValue = vertexValues[0];
        double MaxValue = MinValue;
        double PreMaxValue = MinValue;
        int Len = vertexValues.length;
        if (Len < 2) return null;
        for (int i = 1; i < Len; ++i) {
            double CurrValue = vertexValues[i];
            if (CurrValue < MinValue) {
                MinValue = CurrValue;
                aMinIndex = i;
            } else if (CurrValue > MaxValue) {
                MaxValue = CurrValue;
                aMaxIndex = i;
            }
        }
        PreMaxValue = MinValue;
        aPreMaxIndex = aMinIndex;
        for (int i = 0; i < Len; ++i) {
            if (i == aMaxIndex)
                continue;
            double CurrValue = vertexValues[i];
            if ((CurrValue <= MaxValue) && (CurrValue > PreMaxValue)) {
                PreMaxValue = CurrValue;
                aPreMaxIndex = i;
            }
        }
        return new int[]{aMinIndex, aMaxIndex, aPreMaxIndex};
    }

    private static SimplexResult SimplexMethodDim(SimplexParameters parameters, double[][] simplexVertices, IFunction function) {
        int iter = 0;
        double stdErr = 0;
        double[][] reducedSimplVert;
        int[] idxes = new int[3];
        do {
            SimplexDimReducer sdr = new SimplexDimReducer(simplexVertices, function);
            reducedSimplVert = sdr.getReducedParams();
            double[] funcOnSimpl = sdr.getReducedValues();
            idxes[minInd] = 0;
            idxes[maxInd] = funcOnSimpl.length - 1;
            idxes[preMaxInd] = idxes[maxInd] - 1;
            stdErr = SimplexStep(parameters.getStepParameters(), reducedSimplVert, funcOnSimpl, idxes, function);
            ++iter;
        } while ((iter <= parameters.getNumIterations()) && (stdErr > parameters.getMaxAbsErr()));
        double[] optimalParams = reducedSimplVert[idxes[minInd]];
        double minFuncValue = function.calc(optimalParams);
        return new SimplexResult(minFuncValue, optimalParams, iter);
    }

    private static double SimplexFuncSE(double[] funcOnSimplex) {
        int Len = funcOnSimplex.length;
        if (Len < 2) return 0;
        double avrgVal = 0;
        for (int i = 0; i < Len; ++i)
            avrgVal += funcOnSimplex[i];
        avrgVal /= Len;
        double result = 0;
        for (int i = 0; i < Len; ++i) {
            double elem = funcOnSimplex[i] - avrgVal;
            result += (elem * elem);
        }
        result = Math.sqrt(result / (Len - 1));
        return result;
    }

    private static void ShrinkSimplex(double[][] simplexVert, double shrinkCoeff, int minIdx) {
        for (int i = 0; i < simplexVert.length; ++i)
            if (i != minIdx)
                simplexVert[i] = LinearTransformVector(simplexVert[minIdx], simplexVert[i], shrinkCoeff);
    }

    private static double[] FuncOnSimplex(double[][] simplexVert, IFunction function) {
        double[] result = new double[simplexVert.length];
        for (int i = 0; i < simplexVert.length; ++i)
            result[i] = function.calc(simplexVert[i]);
        return result;
    }

    private static class SimplexDimReducer {
        private double[][] _reducedParams;
        private double[] _reducedValues;

        public SimplexDimReducer(double[][] simplexVertices, IFunction function) {
            _reducedParams = simplexVertices;
            int Len = simplexVertices.length;
            int aDim = SimplexDim(simplexVertices);
            double[] currFuncOnSimpl = new double[Len];
            int[] indexArray = new int[Len];
            for (int i = 0; i < Len; ++i) {
                currFuncOnSimpl[i] = function.calc(simplexVertices[i]);
                indexArray[i] = i;
            }
            UtilsSvd.sort(currFuncOnSimpl, indexArray, false, 0, Len - 1);
            _reducedParams = new double[aDim + 1][];
            _reducedValues = new double[aDim + 1];
            for (int i = 0; i <= aDim; ++i) {
                _reducedParams[i] = simplexVertices[indexArray[i]];
                _reducedValues[i] = currFuncOnSimpl[i];
            }
        }

        static private int SimplexDim(double[][] simplexVertices) {
            int ma = 4;
            int result = ma;
            for (int i = 0; i < ma; ++i)
                if (IsEqualCoord(simplexVertices, i))
                    --result;
            return result;
        }

        static private boolean IsEqualCoord(double[][] simplexVertices, int coordinateInd) {
            for (int i = 1; i < simplexVertices.length; ++i) {
                if (Math.abs(simplexVertices[0][coordinateInd] - simplexVertices[i][coordinateInd]) > Double.MIN_VALUE) {
                    return false;
                }
            }
            return true;
        }

        public double[][] getReducedParams() {
            return _reducedParams;
        }

        public double[] getReducedValues() {
            return _reducedValues;
        }
    }
}
