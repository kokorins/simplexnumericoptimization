package simplex.parameters;

public class SimplexStepParameters {
    public final double reflCoeff;
    public final double expandCoeff;
    public final double contractCoeff;
    public final double shrinkCoeff;
    SimplexStepParameters(double reflectCoef, double expandCoef, double contractCoef, double shrinkCoef) {
        reflCoeff = reflectCoef;
        expandCoeff = expandCoef;
        contractCoeff = contractCoef;
        shrinkCoeff = shrinkCoef;
    }
}
