package simplex.parameters;

public class SimplexParameters {
    private final SimplexStepParameters _stepParameters;

    private final int _numIterations;
    private final double _maxAbsErr;

    SimplexParameters(SimplexStepParameters stepParameters, int numIterations, double maxAbsErr) {
        _stepParameters = stepParameters;
        _numIterations = numIterations;
        _maxAbsErr = maxAbsErr;
    }

    public SimplexStepParameters getStepParameters() {
        return _stepParameters;
    }

    public int getNumIterations() {
        return _numIterations;
    }
    public double getMaxAbsErr() {
        return _maxAbsErr;
    }
}
