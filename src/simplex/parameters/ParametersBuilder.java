package simplex.parameters;

public class ParametersBuilder {
    double reflCoeff = 1;
    double expandCoeff = 2;
    double contractCoeff = 0.5;
    double shrinkCoeff = 0.5;
    double maxAbsErr =1e-6;
    int numIter = 200;
    int numSteps = 20;
    public double eps = 0.05;
    public double paramBandWidth = 0.01;
    public boolean isMedianResRobustBandwidth = false;
    public boolean checkOutliers = true;
    public int robustRecalcIter = 10000;
    public int numRobustIterations = 20;
    public double sigLevel = 1e-3;

    public MultiStepParameters buildMultiStep() {
        return new MultiStepParameters(this);
    }

    public RobustSimplexParameters buildRobust() {
        return new RobustSimplexParameters(this);
    }
}
