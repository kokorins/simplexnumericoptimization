package simplex.averager;

public interface IWeightedSimplexAverage {
    public double[] averageVect(double[][] simplexVert, double[] funcOnSimpex, int ignoreInd);
}
