package simplex;

import junit.framework.Assert;
import org.apache.commons.math3.util.Pair;
import org.junit.Test;
import simplex.function.IFitter;
import simplex.function.IFunction;
import simplex.function.StdErrHillCurveWeightedFittingFunction;

import java.util.Arrays;

public class SimplexOptimizationTest {
    @Test
    public void testSimplex() {
        SimplexOptimization so = new SimplexOptimization();
        double [] x = new double[] {-2, -1, 0, 1, 2};
        double [] y = new double[x.length];
        for(int i=0; i<y.length; ++i)
            y[i] = x[i]*x[i];
        DataFrame data = new DataFrame(x, y);
        double[] initParams = new double[]{1,1,1};
        IFitter fitter = new IFitter() {
            @Override
            public double calc(double x, double[] params) {
                return params[0]*x*x+params[1]*x+params[2];
            }
        };
        double[] sigmas = new double[x.length];
        Arrays.fill(sigmas, 1);
        Simplex mss = new MultiStepSimplex();
        IFunction function = new StdErrHillCurveWeightedFittingFunction(fitter, data, sigmas);
        SimplexResult optParams = mss.calc(initParams, function);
        Assert.assertEquals(0, optParams.getOptValue(), 1e-3);
        double[] params = optParams.getOptParams();
        Assert.assertEquals(1, params[0], 1e-3);
        Assert.assertEquals(0, params[1], 1e-3);
        Assert.assertEquals(0, params[2], 1e-3);
    }
}