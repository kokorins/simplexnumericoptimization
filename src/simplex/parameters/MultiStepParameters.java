package simplex.parameters;

public class MultiStepParameters {
    private final int _numSteps;
    private final SimplexParameters _simplexParameters;

    public MultiStepParameters(ParametersBuilder pb) {
        _simplexParameters = new SimplexParameters(new SimplexStepParameters(pb.reflCoeff, pb.expandCoeff, pb.contractCoeff, pb.shrinkCoeff), pb.numIter, pb.maxAbsErr);
        _numSteps = pb.numSteps;
    }

    public SimplexParameters getSimplexParameters() {
        return _simplexParameters;
    }

    public int getNumSteps() {
        return _numSteps;
    }
}
