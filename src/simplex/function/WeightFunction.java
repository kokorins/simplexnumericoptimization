package simplex.function;

public enum WeightFunction {
    TukeyBiweight {
        /**
         *
         * @param x a value
         * @param width non zero area
         * @return Tukey Biweight function: (1 - (x/width)І)І, |x| < width
         */
        public double getWeight(double x, double width) {
            x /= width;
            x *= x;
            if (x < 1) {
                double temp = 1 - x;
                return temp * temp;
            } else {
                return 0;
            }
        }},
    ClevelandWeight {
        /**
         * @param x a value
         * @param width non zero area
         * @return Cleveland weight function:
         * (1 - xІ/width)І, xІ < width
         */
        public double getWeight(double x, double width) {
            x *= x;
            if (x < width) {
                double temp = 1 - x / width;
                return temp * temp;
            } else {
                return 0;
            }
        }};

    public abstract double getWeight(double x, double width);
}
