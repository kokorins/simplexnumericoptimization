package simplex.function;

import org.apache.commons.lang3.ArrayUtils;

import java.util.List;

public abstract class IFunction {
    public abstract double calc(double[] x);
    public double calc(List<Double> x) {
        Double[] xLoc = new Double[x.size()];
        return calc(ArrayUtils.toPrimitive(x.toArray(xLoc)));
    }
}
