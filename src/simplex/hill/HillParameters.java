package simplex.hill;

public class HillParameters {
    private double aZero;
    private double aInf;
    private double logAC50;
    private double hillCoeff;

    public HillParameters(double azero, double ainf, double logAC50, double hillCoeff) {
        aZero = azero;
        aInf = ainf;
        this.logAC50 = logAC50;
        this.hillCoeff = hillCoeff;
    }

    public void setAZero(double aZero) {
        this.aZero = aZero;
    }

    public double getAZero() {
        return aZero;
    }

    public void setAInf(double aInf) {
        this.aInf = aInf;
    }

    public double getAInf() {
        return aInf;
    }

    public void setLogAC50(double logAC50) {
        this.logAC50 = logAC50;
    }

    public double getLogAC50() {
        return logAC50;
    }

    public void setHillCoeff(double hillCoeff) {
        this.hillCoeff = hillCoeff;
    }

    public double getHillCoeff() {
        return hillCoeff;
    }

}
