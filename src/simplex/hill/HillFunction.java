package simplex.hill;

import org.apache.commons.math3.stat.descriptive.rank.Median;
import simplex.DataFrame;

public class HillFunction {

    /**
     * @param x an argument value (not logarithm)
     * @param hillParameters the function parameters
     * @return The value of 4-paramater logistic function (4PL), or Hill-curve:
     * <BR>
     * f(x) = Ainf + (Azero - Ainf) / (1 + 10^((Log(x) - Log(AC50))*HillCoeff))
     */
    public static double function(double x, HillParameters hillParameters) {
        x = Math.max(1e-30, x);
        return functionOfLogArg(Math.log10(x), hillParameters);
    }

    /**
     * @param x an argument logarithm value
     * @param hillParameters the function parameters
     * @return The value of 4-paramater logistic function (4PL), or Hill-curve:
     * <BR>
     * f(x) = Ainf + (Azero - Ainf) / (1 + 10^((x - Log(AC50))*HillCoeff))
     */
    public static double functionOfLogArg(double x, HillParameters hillParameters) {
        double temp;
        temp = (x - hillParameters.getLogAC50()) * hillParameters.getHillCoeff();
        temp = Math.max(Math.min(temp, 30), -30);
        return hillParameters.getAInf() + (hillParameters.getAZero()
                        - hillParameters.getAInf()) / (1 + Math.pow(10, temp));
    }

    public static double[] rSquaredForHillCurve(DataFrame compoundData,
                                                HillParameters hillParameters, double[] dataWeights) {
        // R squared = Summ((Yfit[i] - Y[i])^2) / Summ((Y[i] - Yavrg)^2)
        // ChiSquare = Sqrt(Summ((Yfit[i] - Y[i])^2) / DF)
        int dataCount = compoundData.size();
        double[] concentrations = compoundData.x();
        double[] activities = compoundData.y();
        double yMean = 0; // activity mean
        for(int i = 0; i < dataCount; i++) {
            yMean += activities[i];
        }
        yMean = yMean / dataCount;
        double resSummSq = 0;
        double summ2 = 0;
        double[] absResidual = new double[dataCount];
        for(int i = 0; i < dataCount; i++) {
            double x = concentrations[i];
            double y = activities[i];
            double hillValue = function(x, hillParameters);
            double temp = hillValue - y;
            resSummSq += temp * temp * dataWeights[i+1];
            temp = y - yMean;
            summ2 += temp * temp * dataWeights[i+1];
            absResidual[i] = Math.abs(hillValue - y);
        }
        // Standard error calculated by the median of absolute values of residuals
        Median median = new Median();
        median.setData(absResidual);
        double medStndErr = 1.4826 * median.evaluate();
//        double medStndErr = 1.4826 * UtilsSpl.medianOfArray(absResidual, dataCount);
        double rSquared = (summ2 <= 0) ? 1 : Math.max(0, 1 - resSummSq / summ2);
        // chiSquared = resSummSq;
        return new double[] { rSquared, resSummSq, medStndErr };
    }

}
