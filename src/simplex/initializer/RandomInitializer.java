package simplex.initializer;

import java.util.Random;

public class RandomInitializer implements SimplexInitializer {
    private Random rand = new Random();

    public RandomInitializer() {
        rand = new Random();
    }

    public RandomInitializer(long seed) {
        rand = new Random(seed);
    }

    public double[][] initSimplex(double[] initParams) {
        double[][] result = new double[initParams.length + 1][];
        for (int i = 0; i < result.length; ++i) {
            if (i == 0)
                result[0] = initParams.clone();
            else
                result[i] = new double[initParams.length];
        }
        for (int i = 1; i <= initParams.length; ++i) {
            for (int j = 0; j < initParams.length; ++j) {
                result[i][j] = initParams[j] + rand.nextGaussian();
            }
        }
        return result;
    }
}
