package simplex.parameters;

public class RobustSimplexParameters {
    private final MultiStepParameters _multiStepParameters;
    private final RobustWeightsParameters _robustWeightsParameters;
    private final double _eps;
    private final int _numRobustIterations;

    public RobustSimplexParameters(ParametersBuilder pb) {
        _multiStepParameters = new MultiStepParameters(pb);
        _eps = pb.eps;
        _numRobustIterations = pb.numRobustIterations;
        _robustWeightsParameters =  new RobustWeightsParameters(pb.paramBandWidth, pb.isMedianResRobustBandwidth, pb.checkOutliers, pb.robustRecalcIter, pb.sigLevel);
    }

    public MultiStepParameters getMultiStepParameters() {
        return _multiStepParameters;
    }

    public double getEps() {
        return _eps;
    }

    public RobustWeightsParameters getRobustWeightsParameters() {
        return _robustWeightsParameters;
    }

    public int getNumRobustIterations() {
        return _numRobustIterations;
    }
}
