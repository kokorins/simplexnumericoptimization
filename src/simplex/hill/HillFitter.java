package simplex.hill;

import simplex.function.IFitter;

public class HillFitter implements IFitter {
    @Override
    public double calc(double x, double[] params) {
        return HillFunction.functionOfLogArg(x, new HillParameters(params[0], params[1], params[2], params[3]));
    }
}
