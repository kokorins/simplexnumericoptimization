package simplex;

import simplex.function.IFunction;
import simplex.initializer.RandomInitializer;
import simplex.initializer.SimplexInitializer;
import simplex.parameters.MultiStepParameters;
import simplex.parameters.ParametersBuilder;

public class MultiStepSimplex implements Simplex {
    private final MultiStepParameters _parameters;

    public MultiStepSimplex() {
        _parameters = new ParametersBuilder().buildMultiStep();
    }
    public MultiStepSimplex(MultiStepParameters parameters) {
        _parameters = parameters;
    }
    @Override
    public SimplexResult calc(double[] initPoint, IFunction function) {
        return MultiStepSimplexMethod(_parameters, initPoint, function);
    }

    public static SimplexResult MultiStepSimplexMethod(MultiStepParameters params, double[] initPoint, IFunction function) {
        SimplexInitializer initializer = new RandomInitializer(1);
        double[][] simplexVert = initializer.initSimplex(initPoint);
        SimplexResult optVals = SimplexOptimization.SimplexMethod(params.getSimplexParameters(), simplexVert, function);
        for (int i = 1; i < params.getNumSteps(); ++i) {
            simplexVert = initializer.initSimplex(initPoint);
            SimplexResult curVal = SimplexOptimization.SimplexMethod(params.getSimplexParameters(), simplexVert, function);
            if (curVal.getOptValue() < optVals.getOptValue())
                optVals = curVal;
        }
        return optVals;
    }
}
