package simplex;

import org.apache.commons.math3.stat.descriptive.rank.Median;
import simplex.function.IFitter;
import simplex.function.IFunction;
import simplex.function.WeightFunction;
import simplex.parameters.ParametersBuilder;
import simplex.parameters.RobustSimplexParameters;
import simplex.parameters.RobustWeightsParameters;

import java.util.Arrays;

public class RobustSimplex implements Simplex {
    private final RobustSimplexParameters _parameters;
    private DataFrame _data;
    private IFitter _fitter;

    public RobustSimplex(DataFrame data, IFitter fitter) {
        _parameters = new ParametersBuilder().buildRobust();
        _data = data;
        _fitter = fitter;
    }
    public RobustSimplex(RobustSimplexParameters parameters, DataFrame data, IFitter fitter) {
        _parameters = parameters;
        _data = data;
        _fitter = fitter;
    }
    @Override
    public SimplexResult calc(double[] initPoint, IFunction function) {
        return RobustSimplexMethod(_parameters, _data, initPoint, _fitter, function);
    }

    public static SimplexResult RobustSimplexMethod(RobustSimplexParameters params,
                                                    DataFrame data, double[] initPoint,
                                                    IFitter fitter, IFunction function) {
        double[] sigmas = new double[data.size()];
        Arrays.fill(sigmas, 1);
        SimplexResult curOptParams = new SimplexResult(Double.NaN, new double[]{}, 0);
        double OldMedianRes = 0;
        for (int k = 0; k < params.getNumRobustIterations(); ++k) {
            curOptParams = MultiStepSimplex.MultiStepSimplexMethod(params.getMultiStepParameters(), initPoint, function);
            double currMedianRes = ParamRobustWeight(params.getRobustWeightsParameters(), curOptParams.getOptParams(), data, sigmas, fitter);
            double DifMedianRes = Math.abs(currMedianRes - OldMedianRes);
            if ((DifMedianRes < params.getEps()) || ((k >= params.getNumRobustIterations()) && ((DifMedianRes / (currMedianRes + params.getEps())) > (1 - params.getEps()))))
                break;
            else
                OldMedianRes = currMedianRes;
        }
        return curOptParams;
    }

    public static double ParamRobustWeight(RobustWeightsParameters params, double[] a, DataFrame data,
                                           double[] sigmas, IFitter fitter) {
        double currMedianRes;
        double normConst;
        double[] residuals;
        residuals = new double[data.size()];
        // Using Median|Res|
        if (params.isMedianResRobustBandwidth) {
            for (int i = 0; i < data.size(); i++) {
                residuals[i] = Math.abs(data.y()[i] - fitter.calc(data.x()[i], a));
            }
            Median median = new Median();
            median.setData(residuals);
            currMedianRes = median.evaluate();
        }
        // Using MAD = Median|Res-Median(Res)|
        else {
            for (int i = 0; i < data.size(); i++) {
                residuals[i] = data.y()[i] - fitter.calc(data.x()[i], a);
            }
            currMedianRes = MadOfArray(residuals);
        }
        normConst = params.paramBandWidth * currMedianRes;
        int counter = params.robustRecalcIter;
        do {
            for (int i = 0; i < data.size(); i++) {
                // Recalculate the robust weight by the current residuals
                sigmas[i] = WeightFunction.TukeyBiweight.getWeight(residuals[i] / normConst, 1);
            }
            if (!params.checkOutliers)
                break;
            normConst *= 1.5;
            --counter;
        }
        while (OutliersCount(sigmas, params.sigLevel) >= 0.6 * Math.pow(data.size(), 0.65) && counter > 0);
        return currMedianRes;
    }

    private static double MadOfArray(double[] vals) {
        Median md = new Median();
        double med = md.evaluate(vals);
        double[] disc = vals.clone();
        for(int i=0; i<disc.length; ++i)
            disc[i] = Math.abs(vals[i]-med);
        return 1.4826*md.evaluate(disc);
    }

    private static int OutliersCount(double[] weights, double significiantLevel) {
        int res = 0;
        for (int i = 1; i < weights.length; ++i) {
            if (weights[i] < significiantLevel)
                ++res;
        }
        return res;
    }

}
