package simplex;

import simplex.function.IFunction;

public interface Simplex {
    SimplexResult calc(double[] initPoint, IFunction function);
}
