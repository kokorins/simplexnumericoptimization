package simplex;

import java.util.Arrays;

public class UtilsSvd {

    private static int partititionByFirstElement(double[] ar, boolean descending, int begIndex, int endIndex) {
        // Partition and rearrange the array - split it to low and hihg parts:
        // ar[begIndex]..ar[result] <= separate value (ar[begIndex]) and
        // ar[result + 1]..ar[endIndex] >= separate value (ar[begIndex])
        double temp;
        double separateValue = ar[begIndex];
        int leftIndex = begIndex - 1;
        int rightIndex = endIndex + 1;
        while (true) {
            if (descending) {
                // Descending sorting:
                // Looking up the last element withs value not less than the separate value
                do {
                    rightIndex--;
                } while (ar[rightIndex] < separateValue);
                // Looking up the first element withs value not greater than the separate value
                do {
                    leftIndex++;
                } while (ar[leftIndex] > separateValue);
            } else {
                // Ascending sorting:
                // Looking up the last element withs value not greater than the separate value
                do {
                    rightIndex--;
                } while (ar[rightIndex] > separateValue);
                // Looking up the first element withs value not less than the separate value
                do {
                    leftIndex++;
                } while (ar[leftIndex] < separateValue);
            }
            if (leftIndex < rightIndex) {
                // Swap left and right elements
                temp = ar[leftIndex];
                ar[leftIndex] = ar[rightIndex];
                ar[rightIndex] = temp;
            } else {
                return rightIndex;
            }
        }
    }

    public static void sort(double[] ar, boolean descending, int begIndex, int endIndex) {
        // Subarray inplace sorting.
        // Subarray defined by begin and end indices. Both included.
        // The sort order defined by the parameter.

//        //if(ar.length<2){ return; }
//        if (endIndex <= begIndex) {
//            return;
//        }
//        int separateIndex = partititionByFirstElement(ar, descending, begIndex, endIndex);
//        sort(ar, descending, begIndex, separateIndex);
//        sort(ar, descending, separateIndex + 1, endIndex);
        if (descending) {
            for (int i = 0; i < ar.length; ++i)
                ar[i] = -ar[i];
        }
        Arrays.sort(ar, begIndex, endIndex + 1);
        if (descending) {
            for (int i = 0; i < ar.length; ++i)
                ar[i] = -ar[i];
        }
    }

    private static int partititionByFirstElement(double[] ar, int[] indexArray, boolean descending,
                                                 int begIndex, int endIndex) {
        // Partition and rearrange the array - split it to low and hihg parts:
        // ar[begIndex]..ar[result] <= separate value (ar[begIndex]) and
        // ar[result + 1]..ar[endIndex] >= separate value (ar[begIndex])
        double temp;
        int tempInd;
        double separateValue = ar[begIndex];
        int leftIndex = begIndex - 1;
        int rightIndex = endIndex + 1;
        while (true) {
            if (descending) {
                // Descending sorting:
                // Looking up the last element withs value not less than the separate value
                do {
                    rightIndex--;
                } while (ar[rightIndex] < separateValue);
                // Looking up the first element withs value not greater than the separate value
                do {
                    leftIndex++;
                } while (ar[leftIndex] > separateValue);
            } else {
                // Ascending sorting:
                // Looking up the last element withs value not greater than the separate value
                do {
                    rightIndex--;
                } while (ar[rightIndex] > separateValue);
                // Looking up the first element withs value not less than the separate value
                do {
                    leftIndex++;
                } while (ar[leftIndex] < separateValue);
            }
            if (leftIndex < rightIndex) {
                // Swap left and right elements
                temp = ar[leftIndex];
                ar[leftIndex] = ar[rightIndex];
                ar[rightIndex] = temp;
                tempInd = indexArray[leftIndex];
                indexArray[leftIndex] = indexArray[rightIndex];
                indexArray[rightIndex] = tempInd;
            } else {
                return rightIndex;
            }
        }
    }

    public static void sort(double[] ar, int[] indexArray, boolean descending,
                            int begIndex, int endIndex) {
        // Subarrays inplace cosorting.
        // The array sorted by values and the indices array rearranged accordingly.
        // Subarrays defined by begin and end indices. Both included.
        // The sort order defined by the parameter.

        //if(ar.length<2){ return; }
        if (endIndex <= begIndex) {
            return;
        }
        int separateIndex = partititionByFirstElement(ar, indexArray, descending, begIndex, endIndex);
        sort(ar, indexArray, descending, begIndex, separateIndex);
        sort(ar, indexArray, descending, separateIndex + 1, endIndex);
    }

    /**
     * Computes Sqrt(a*a + b*b) without destructive underflow or overflow
     *
     * @return Sqrt(a*a + b*b)
     */
    public static double sqrtSS(double a, double b) {
        // Could be replaced with Math.hypot(a,b) but the last is slower!
        double absa, absb;
        absa = Math.abs(a);
        absb = Math.abs(b);
        if (absa > absb) {
            absb /= absa;
            absb *= absb;
            return absa * Math.sqrt(1 + absb);
        }
        if (absb < 1E-20) {
            return 0;
        }
        absa /= absb;
        absa *= absa;
        return absb * Math.sqrt(1 + absa);
    }

    public static void sortSVDMartices(double[][] a, double[] w, double[][] v, int firstIndex) {
        // Cosorting array W and columns of matrices A and V in the W values descendig order
        int columnsCount = w.length;
        int aRowsCount = a.length;
        int vRowsCount = v.length;
        int[] indexArray;
        indexArray = new int[columnsCount];
        for (int i = firstIndex; i < columnsCount; i++) {
            indexArray[i] = i;
        }
        // Sort the array
        sort(w, indexArray, true, firstIndex, columnsCount - 1);
        // Swap the matrices columns
        double[] temp, ari;
        temp = new double[columnsCount + 1];
        for (int i = firstIndex; i < aRowsCount; i++) {
            ari = a[i];
            for (int j = firstIndex; j < columnsCount; j++) {
                temp[j] = ari[j];
            }
            for (int j = firstIndex; j < columnsCount; j++) {
                ari[j] = temp[indexArray[j]];
            }
        }
        for (int i = firstIndex; i < vRowsCount; i++) {
            ari = v[i];
            for (int j = firstIndex; j < columnsCount; j++) {
                temp[j] = ari[j];
            }
            for (int j = firstIndex; j < columnsCount; j++) {
                ari[j] = temp[indexArray[j]];
            }
        }
    }
}
