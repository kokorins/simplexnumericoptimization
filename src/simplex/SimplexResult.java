package simplex;

public class SimplexResult {
    final private double _optValue;
    final private double[] _optParams;
    final private int _numIter;

    public SimplexResult(double optValue, double[] optParams, int numIter) {
        _optValue = optValue;
        _optParams = optParams;
        _numIter = numIter;
    }

    public double getOptValue() {
        return _optValue;
    }

    public double[] getOptParams() {
        return _optParams;
    }

    public int getNumIter() {
        return _numIter;
    }
}
