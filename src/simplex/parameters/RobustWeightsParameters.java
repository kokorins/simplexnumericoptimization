package simplex.parameters;

public class RobustWeightsParameters {
    public final double paramBandWidth;
    public final boolean isMedianResRobustBandwidth;
    public final boolean checkOutliers;
    public final int robustRecalcIter;
    public final double sigLevel;

    public RobustWeightsParameters(double paramBandWidth_, boolean isMedianResRobustBandwidth_, boolean checkOutliers_, int robustRecalcIter_, double sigLevel_) {
        paramBandWidth = paramBandWidth_;
        isMedianResRobustBandwidth = isMedianResRobustBandwidth_;
        checkOutliers = checkOutliers_;
        robustRecalcIter = robustRecalcIter_;
        sigLevel = sigLevel_;
    }

}
