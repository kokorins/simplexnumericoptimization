package simplex.function;

public interface IFitter {
    double calc(double x, double[] params);
}
