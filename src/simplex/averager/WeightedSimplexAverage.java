package simplex.averager;

import simplex.averager.IWeightedSimplexAverage;

public class WeightedSimplexAverage implements IWeightedSimplexAverage {
    public double[] averageVect(double[][] simplexVert, double[] funcOnSimpex, int ignoreInd) {
        int Len = simplexVert.length - 1;
        int Len1 = simplexVert[0].length;
        double[] result = new double[Len1];
        for (int j = 0; j <= Len1 - 1; ++j)
            result[j] = 0;
        double aSummWeight = 0;
        for (int i = 0; i <= Len; ++i) {
            if (i == ignoreInd) continue;
            double aWeight = 1 / funcOnSimpex[i];
            aSummWeight += aWeight;
            for (int j = 0; j <= Len1 - 1; ++j)
                result[j] += aWeight * simplexVert[i][j];
        }
        for (int j = 0; j <= Len1 - 1; ++j)
            result[j] /= aSummWeight;
        return result;
    }
}
