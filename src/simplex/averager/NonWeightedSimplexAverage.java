package simplex.averager;

public class NonWeightedSimplexAverage implements IWeightedSimplexAverage {
    public double[] averageVect(double[][] simplexVert, double[] funcOnSimpex, int ignoreInd) {
        int Len = simplexVert.length - 1;
        int Len1 = simplexVert[0].length;
        double[] result = new double[Len1];
        for (int j = 0; j <= Len1 - 1; ++j)
            result[j] = 0;
        for (int i = 0; i <= Len; ++i) {
            if (i == ignoreInd) continue;
            for (int j = 0; j <= Len1 - 1; ++j)
                result[j] += simplexVert[i][j];
        }
        for (int j = 0; j <= Len1 - 1; ++j)
            result[j] /= Len;
        return result;
    }
}
